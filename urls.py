from django.urls import path
from . import views

app_name = 'birds'
urlpatterns = [
    path('', views.index, name='index'),
    path('register/', views.register, name='register'),
    path('login/', views.login, name='login'),
    path('logout/', views.logout, name='logout'),
    path('birds/', views.birds, name='birds'),
    path('eggs/', views.eggs, name='eggs'),
    path('sell/', views.sell, name='sell'),
    path('exchange/', views.exchange, name='exchange'),
]
