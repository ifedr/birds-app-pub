from django.contrib import admin

from .models import BirdTemplate, Player

admin.site.register(BirdTemplate)
admin.site.register(Player)
