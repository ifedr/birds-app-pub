from django.db import models
from django.utils import timezone


class Player(models.Model):
    name = models.CharField(max_length=20)
    password = models.CharField(max_length=20)
    gold = models.FloatField(default=0)
    silver = models.FloatField(default=0)
    eggs = models.PositiveIntegerField(default=0)

    def __str__(self):
        return self.name

    def egg_price(self):
        return 0.0001 # gold

    def birds_amount(self, bird_t):
        bird = self.birds_set.filter(template=bird_t).first()
        amount = bird.amount if bird else 0
        return amount

    def get_bird(self, bird_t):
        bird = self.birds_set.filter(template=bird_t).first()
        if bird:
            return bird
        else:
            return Birds(template=bird_t, player=self, amount=0, last_egg=timezone.now())

    def get_birds_list(self):
        bt_list = BirdTemplate.objects.all().order_by('cost')
        birds_list = [self.get_bird(bt) for bt in bt_list]
        return birds_list

    def buy_birds(self, bt_id, amount):
        if amount < 1:
            return False
        bt = BirdTemplate.objects.get(id=bt_id)
        # handle silver
        total_cost = amount * bt.cost
        silver_new = self.silver - total_cost
        if silver_new < 0:
            print('Not enough money.')
            return False
        self.silver = silver_new
        self.save()
        # handle birds
        bird = self.birds_set.filter(template=bt).first()
        if bird:
            bird.amount += amount
            bird.save()
        else:
            self.birds_set.create(template=bt, amount=amount, 
                last_egg=timezone.now())
        return True

    def gather_eggs(self):
        counter = 0
        for bird in self.get_birds_list():
            counter += bird._gather_eggs()
        self.eggs += counter
        self.save()

    def sell_eggs(self):
        self.gold += self.eggs * self.egg_price()
        self.eggs = 0
        self.save()
        
    def exchange(self, gold):
        if self.gold < gold:
            return False
        silver = gold * 100
        silver *= 4 # +300% bonus
        self.gold -= gold
        self.silver += silver
        self.save()
        return True
        

class BirdTemplate(models.Model):
    name = models.CharField(max_length=20)
    fertility = models.PositiveIntegerField()
    cost = models.PositiveIntegerField()

    def __str__(self):
        return "%s, %s eggs/h, %s silver" % (self.name, self.fertility, self.cost)


class Birds(models.Model):
    template = models.ForeignKey(BirdTemplate, on_delete=models.CASCADE)
    player = models.ForeignKey(Player, on_delete=models.CASCADE)
    amount = models.PositiveIntegerField(default=0)
    last_egg = models.DateTimeField('time of last egg made')

    def __str__(self):
        return "%s, player: %s, amount: %s" % (self.template.name, self.player.name, self.amount)

    def period(self):
        seconds = 60*60/self.template.fertility
        return timezone.timedelta(seconds=seconds)

    def eggs_available(self, single=False):
        delta = timezone.now() - self.last_egg
        eggs_from_bird = int(delta / self.period())
        if single:
            return eggs_from_bird
        return eggs_from_bird * self.amount

    def _gather_eggs(self):
        eggs_from_bird = self.eggs_available(single=True)
        if eggs_from_bird == 0:
            return 0
        delta = self.period() * eggs_from_bird
        self.last_egg += delta
        self.save()
        return eggs_from_bird * self.amount
