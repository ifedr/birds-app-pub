from django.shortcuts import render, redirect
from django.http import HttpResponse
from .models import Player, BirdTemplate

# helper
def get_player(request):
    pid = request.session.get('player_id', None)
    if pid:
        return Player.objects.filter(id=pid).first()
    else:
        return None

def index(request):
    return render(request, 'birds/index.html', 
        {'player': get_player(request)})

def register(request):
    try:
        name = request.POST['name']
        password = request.POST['password']
        try:
            Player.objects.get(name=name)
            error = "Player with that nickname is already created."
            return render(request, 'birds/register.html', 
                {'error': error})
        except Player.DoesNotExist:
            player = Player(name=name, password=password)
            player.silver = 500.0
            player.save()
            return redirect('birds:index')
    except:
        return render(request, 'birds/register.html')

def login(request):
    try:
        name = request.POST['name']
        password = request.POST['password']
        try:
            player = Player.objects.get(name=name)
            if player.password != password:
                raise Player.DoesNotExist
            request.session['player_id'] = player.id
            return redirect('birds:index')
        except Player.DoesNotExist:
            error = "Wrong nickname or password."
            return render(request, 'birds/login.html', 
                {'error': error})
    except:
        return render(request, 'birds/login.html')

def logout(request):
    try:
        del request.session['player_id']
        request.session.flush()
    except KeyError:
        pass
    return redirect('birds:index')

def birds(request):
    player = get_player(request)
    if not Player:
        return redirect('birds:logout')
    if request.POST:
        bt_id = request.POST['template']
        amount = int(request.POST['amount'])
        player.buy_birds(bt_id, amount)
    birds_list = player.get_birds_list()
    return render(request, 'birds/birds.html',
        {'player': player, 'birds': birds_list})

def eggs(request):
    player = get_player(request)
    if not Player:
        return redirect('birds:logout')
    if request.POST:
        player.gather_eggs()
    birds_list = player.get_birds_list()
    eggs_list = [b.eggs_available() for b in birds_list]
    return render(request, 'birds/eggs.html', {
        'player': player,
        'birds_and_eggs': zip(birds_list, eggs_list)
        })

def sell(request):
    player = get_player(request)
    if not Player:
        return redirect('birds:logout')
    if request.POST:
        player.sell_eggs()
    return render(request, 'birds/sell.html', {
        'player': player, 
        'gold_available': player.eggs * player.egg_price(),
        })

def exchange(request):
    player = get_player(request)
    if not Player:
        return redirect('birds:logout')
    if request.POST:
        try:
            gold = float(request.POST["amount"])
            player.exchange(gold)
        except:
            pass
    return render(request, 'birds/exchange.html', {
        'player': player, 
        })
